﻿namespace LendFoundry.Business.Application
{
    public enum AccountType
    {
        Savings = 1,
        Checking = 2
    }
}