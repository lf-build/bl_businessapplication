﻿namespace LendFoundry.Business.Application
{
    public enum AddressType
    {
        Residence = 1,
        Work = 2
    }
}