﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using Newtonsoft.Json;

namespace LendFoundry.Business.Application
{
    public class Application : Aggregate, IApplication
    {
        public Application()
        {
        }

        public Application(IApplicationRequest applicationRequest)
        {
            ContactFirstName = applicationRequest.ContactFirstName;
            ContactLastName = applicationRequest.ContactLastName;
            RequestedAmount = applicationRequest.RequestedAmount;
            LoanTimeFrame = applicationRequest.LoanTimeFrame;
            RequestedTermType = applicationRequest.RequestedTermType;
            RequestedTermValue = applicationRequest.RequestedTermValue;
            PurposeOfLoan = applicationRequest.PurposeOfLoan;
            Source = applicationRequest.Source;
            ApplicationNumber = applicationRequest.ApplicationNumber;
            ApplicationDate = applicationRequest.ApplicationDate;
            SelfDeclareInformation = applicationRequest.SelfDeclareInformation;
            PrimaryAddress = applicationRequest.PrimaryAddress;
            PrimaryFax = applicationRequest.PrimaryFax;
            PrimaryPhone = applicationRequest.PrimaryPhone;
            PrimaryEmail = applicationRequest.PrimaryEmail;
            DateNeeded = applicationRequest.DateNeeded;
            PropertyType = applicationRequest.PropertyType;
            OtherPurposeDescription = applicationRequest.OtherPurposeDescription;
            ApplicantId = applicationRequest.PrimaryApplicant.Id;
            LeadOwnerId = applicationRequest.LeadOwnerId;
            PartnerUserId = applicationRequest.PartnerUserId;
            PartnerId = applicationRequest.PartnerId;
            LeadOwnerEmail = applicationRequest.LeadOwnerEmail;
            ExternalReferences = applicationRequest.ExternalReferences;
            ProductId = applicationRequest.ProductId;
            TotalDrawDownAmount = applicationRequest.TotalDrawDownAmount;
            LoanAmount = applicationRequest.LoanAmount;
            ClientIpAddress = applicationRequest.ClientIpAddress;
            BusinessPhone = applicationRequest.BusinessPhone;
            OtherLenderNotes = applicationRequest.OtherLenderNotes;
            ProductApprovalDate = applicationRequest.ProductApprovalDate;
            ParentApplicationNumber = applicationRequest.ParentApplicationNumber;
            SequenceNumber = applicationRequest.SequenceNumber;
            RenewalAmount = applicationRequest.RenewalAmount;
            RenewedOn = applicationRequest.RenewedOn;
            RenewedBy = applicationRequest.RenewedBy;
            RenewalSource = applicationRequest.RenewalSource;
            ApplicationType = applicationRequest.ApplicationType;
            OriginalParentApplicationNumber = applicationRequest.OriginalParentApplicationNumber;
        }

        public string OldProductId { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public double RequestedAmount { get; set; }
        public string LoanTimeFrame { get; set; }
        public string RequestedTermType { get; set; }
        public double RequestedTermValue { get; set; }
        public string PurposeOfLoan { get; set; }
        public string OtherPurposeDescription { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ISource, Source>))]
        public ISource Source { get; set; }

        public string ApplicationNumber { get; set; }
        public string Signature { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ISelfDeclareInformation, SelfDeclareInformation>))]
        public ISelfDeclareInformation SelfDeclareInformation { get; set; }

        public TimeBucket ApplicationDate { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IBankInformation, BankInformation>))]
        public IBankInformation LinkedBankInformation { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IEmailAddress, EmailAddress>))]
        public IEmailAddress LinkedEmailAddress { get; set; }

        public TimeBucket ExpiryDate { get; set; }

        public TimeBucket DecisionDate { get; set; } //- Approve or Declined

        public string ApplicantId { get; set; }

        public string ProductId { get; set; }

        public string PropertyType { get; set; }
        public List<string> Owners { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPhoneNumber, PhoneNumber>))]
        public IPhoneNumber PrimaryPhone { get; set; }

        public string PrimaryFax { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IEmailAddress, EmailAddress>))]
        public IEmailAddress PrimaryEmail { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress PrimaryAddress { get; set; }

        public string LeadOwnerId { get; set; }
        public string PartnerId { get; set; }
        public string PartnerUserId { get; set; }

        public TimeBucket DateNeeded { get; set; }
        public string LeadOwnerEmail { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IExternalReferences, ExternalReferences>))]
        public List<IExternalReferences> ExternalReferences { get; set; }

        public double TotalDrawDownAmount { get; set; }

        public double LoanAmount { get; set; }
        public string ClientIpAddress { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPhoneNumber, PhoneNumber>))]
        public IPhoneNumber BusinessPhone { get; set; }

        public string PortfolioType { get; set; }
        public string ProductCategory { get; set; }
        public string OldPortfolioType { get; set; }
        public string OldProductCategory { get; set; }

        public string OtherLenderNotes { get; set; }

        public TimeBucket ProductApprovalDate { get; set; }
        public string ParentApplicationNumber { get; set; }
        public int SequenceNumber { get; set; }
        public double RenewalAmount { get; set; }
        public TimeBucket RenewedOn { get; set; }
        public string RenewedBy { get; set; }
        public string RenewalSource { get; set; }
        public string ApplicationType { get; set; }
        public string OriginalParentApplicationNumber { get; set; }
        public string BusinessWebsite{get;set;}
    }
}