﻿using LendFoundry.Business.Applicant;

namespace LendFoundry.Business.Application
{
    public class ApplicationExtension : IApplicationExtension
    {
        public IApplication Application { get; set; }
        public IApplicant Applicant { get; set; }
        public string StatusWorkFlowId { get; set; }
    }
}