﻿using System.Collections.Generic;
using LendFoundry.Business.Applicant;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Date;
using Newtonsoft.Json;

namespace LendFoundry.Business.Application
{
    public class ApplicationRequest : IApplicationRequest
    {
        public string ProductId { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IApplicantRequest, ApplicantRequest>))]
        public IApplicantRequest PrimaryApplicant { get; set; }

        public double RequestedAmount { get; set; }
        public string RequestedTermType { get; set; }
        public double RequestedTermValue { get; set; }
        public string PurposeOfLoan { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ISource, Source>))]
        public ISource Source { get; set; }

        public string ApplicationNumber { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public string LoanTimeFrame { get; set; }

        public string PropertyType { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ISelfDeclareInformation, SelfDeclareInformation>))]
        public ISelfDeclareInformation SelfDeclareInformation { get; set; }

        public TimeBucket ApplicationDate { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPhoneNumber, PhoneNumber>))]
        public IPhoneNumber PrimaryPhone { get; set; }

        public string PrimaryFax { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IEmailAddress, EmailAddress>))]
        public IEmailAddress PrimaryEmail { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress PrimaryAddress { get; set; }

        public TimeBucket DateNeeded { get; set; }
        public string Signature { get; set; }
        public string LeadOwnerId { get; set; }
        public string PartnerId { get; set; }
        public string PartnerUserId { get; set; }
        public string OtherPurposeDescription { get; set; }
        public string LeadOwnerEmail { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IExternalReferences, ExternalReferences>))]
        public List<IExternalReferences> ExternalReferences { get; set; }

        public double TotalDrawDownAmount { get; set; }

        public double LoanAmount { get; set; }
        public string ClientIpAddress { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPhoneNumber, PhoneNumber>))]
        public IPhoneNumber BusinessPhone { get; set; }

        public string OtherLenderNotes { get; set; }

        public TimeBucket ProductApprovalDate { get; set; }
        public string ParentApplicationNumber { get; set; }
        public int SequenceNumber { get; set; }
        public double RenewalAmount { get; set; }
        public TimeBucket RenewedOn { get; set; }
        public string RenewedBy { get; set; }
        public string RenewalSource { get; set; }
        public string ApplicationType { get; set; }
        public string OriginalParentApplicationNumber { get; set; }

        public string PortfolioType { get; set; }
        public string ProductCategory { get; set; }
        public string OldPortfolioType { get; set; }
        public string OldProductCategory { get; set; }

    }
}