﻿using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Date;
using Newtonsoft.Json;

namespace LendFoundry.Business.Application
{
    public class ApplicationResponse : IApplicationResponse
    {
        public ApplicationResponse()
        {
        }

        public ApplicationResponse(IApplication application, string workFlowStatusId)
        {
            ApplicationNumber = application.ApplicationNumber;
            RequestedAmount = application.RequestedAmount;
            LoanTimeFrame = application.LoanTimeFrame;
            RequestedTermType = application.RequestedTermType;
            RequestedTermValue = application.RequestedTermValue;
            PurposeOfLoan = application.PurposeOfLoan;
            Source = application.Source;
            ApplicationDate = application.ApplicationDate;
            ExpiryDate = application.ExpiryDate;
            SelfDeclareInformation = application.SelfDeclareInformation ?? null;
            ApplicantId = application.ApplicantId;
            LeadOwnerId = application.LeadOwnerId;
            PartnerId = application.PartnerId;
            PartnerUserId = application.PartnerUserId;
            ProductId = application.ProductId;
            WorkFlowStatusId = workFlowStatusId;
            ApplicationUrl = ApplicationUrl;
        }

        public string LoanTimeFrame { get; set; }
        public double RequestedAmount { get; set; }
        public string RequestedTermType { get; set; }
        public double RequestedTermValue { get; set; }
        public string PurposeOfLoan { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ISource, Source>))]
        public ISource Source { get; set; }

        public string ApplicationNumber { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ISelfDeclareInformation, SelfDeclareInformation>))]
        public ISelfDeclareInformation SelfDeclareInformation { get; set; }

        public TimeBucket ApplicationDate { get; set; }
        public TimeBucket ExpiryDate { get; set; }
        public string ApplicantId { get; set; }
        public string LeadOwnerId { get; set; }
        public string PartnerId { get; set; }
        public string PartnerUserId { get; set; }
        public string ProductId { get; set; }
        public string WorkFlowStatusId { get; set; }
        public string ApplicationUrl { get; set; }
        public object ExtractedField {get; set;}
        public string BusinessLocation {get; set;}
        public string BusinessWebsite {get; set;}
    }
}