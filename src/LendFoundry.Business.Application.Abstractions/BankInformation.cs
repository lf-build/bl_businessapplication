﻿using System;

namespace LendFoundry.Business.Application
{
    public class BankInformation : IBankInformation
    {
        public BankInformation()
        {
        }

        public BankInformation(IBankInformationRequest bankInformation)
        {
            BankId = bankInformation.BankId;
            BankName = bankInformation.BankName;
            AccountHolderName = bankInformation.AccountHolderName;
            AccountNumber = bankInformation.AccountNumber;
            AccountType = bankInformation.AccountType;
            RoutingNumber = bankInformation.RoutingNumber;
        }

        public string BankId { get; set; }
        public string BankName { get; set; }
        public string AccountNumber { get; set; }
        public string AccountHolderName { get; set; }
        public string RoutingNumber { get; set; }
        public AccountType AccountType { get; set; }
        public bool IsVerified { get; set; }

        public bool IsDefault { get; set; }
        public DateTimeOffset VerifiedDate { get; set; }

        public string VerifiedBy { get; set; }
    }
}