﻿namespace LendFoundry.Business.Application
{
    public enum EmailType
    {
        Personal = 1,
        Work = 2
    }
}