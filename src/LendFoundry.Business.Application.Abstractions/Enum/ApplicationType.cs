﻿namespace LendFoundry.Business.Application
{
    public enum ApplicationType
    {
        New = 1,
        Renewal = 2
    }
}