﻿using LendFoundry.Business.Applicant;

namespace LendFoundry.Business.Application.Events
{
    public class ApplicationCreated
    {
        public IApplication Application { get; set; }

        public IApplicant Applicant { get; set; }

        public string EntityId { get; set; }
        public string EntityType { get; set; }
    }
}