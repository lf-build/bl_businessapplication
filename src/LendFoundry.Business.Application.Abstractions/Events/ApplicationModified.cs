﻿using LendFoundry.Business.Applicant;

namespace LendFoundry.Business.Application.Events
{
    public class ApplicationModified
    {
        public IApplication Application { get; set; }

        public IApplicant Applicant { get; set; }
    }
}