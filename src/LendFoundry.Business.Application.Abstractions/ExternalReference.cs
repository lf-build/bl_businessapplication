﻿namespace LendFoundry.Business.Application
{
    public class ExternalReference : IExternalReference
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}