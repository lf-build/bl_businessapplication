﻿using System.Collections.Generic;

namespace LendFoundry.Business.Application
{
    public class ExternalReferences : IExternalReferences
    {
        public string Name { get; set; }
        public List<string> Value { get; set; }
    }
}