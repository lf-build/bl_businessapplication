﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Business.Application
{
    public class ExternalReferencesRequest : IExternalReferencesRequest
    {
        [JsonConverter(typeof(InterfaceListConverter<IExternalReference, ExternalReference>))]
        public List<IExternalReference> ExternalReferences { get; set; }
    }
}