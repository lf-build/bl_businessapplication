﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using Newtonsoft.Json;

namespace LendFoundry.Business.Application
{
    public interface IApplication : IAggregate
    {
        string ApplicantId { get; set; }
        string ContactFirstName { get; set; }
        string ContactLastName { get; set; }
        string LoanTimeFrame { get; set; }

        double RequestedAmount { get; set; }
        string RequestedTermType { get; set; }
        double RequestedTermValue { get; set; }

        TimeBucket ApplicationDate { get; set; }

        TimeBucket DateNeeded { get; set; }

        TimeBucket ExpiryDate { get; set; }

        string ProductId { get; set; }

        string OldProductId { get; set; }

        ISource Source { get; set; }

        string PurposeOfLoan { get; set; }

        string ApplicationNumber { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ISelfDeclareInformation, SelfDeclareInformation>))]
        ISelfDeclareInformation SelfDeclareInformation { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IBankInformation, BankInformation>))]
        IBankInformation LinkedBankInformation { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IEmailAddress, EmailAddress>))]
        IEmailAddress LinkedEmailAddress { get; set; }

        TimeBucket DecisionDate { get; set; } //- Approve or Declined

        List<string> Owners { get; set; }

        string PropertyType { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPhoneNumber, PhoneNumber>))]
        IPhoneNumber PrimaryPhone { get; set; }

        string PrimaryFax { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IEmailAddress, EmailAddress>))]
        IEmailAddress PrimaryEmail { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        IAddress PrimaryAddress { get; set; }

        string Signature { get; set; }
        string LeadOwnerId { get; set; }
        string PartnerId { get; set; }
        string PartnerUserId { get; set; }
        string OtherPurposeDescription { get; set; }
        string LeadOwnerEmail { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IExternalReference, ExternalReference>))]
        List<IExternalReferences> ExternalReferences { get; set; }

        double TotalDrawDownAmount { get; set; }
        double LoanAmount { get; set; }
        string ClientIpAddress { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPhoneNumber, PhoneNumber>))]
        IPhoneNumber BusinessPhone { get; set; }

        string PortfolioType { get; set; }
        string ProductCategory { get; set; }
        string OldPortfolioType { get; set; }
        string OldProductCategory { get; set; }
        string OtherLenderNotes { get; set; }
        TimeBucket ProductApprovalDate { get; set; }
        string ParentApplicationNumber { get; set; }
        int SequenceNumber { get; set; }
        double RenewalAmount { get; set; }
        TimeBucket RenewedOn { get; set; }
        string RenewedBy { get; set; }
        string RenewalSource { get; set; }
        string ApplicationType { get; set; }
        string OriginalParentApplicationNumber { get; set; }
    }
}