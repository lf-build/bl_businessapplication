﻿using LendFoundry.Foundation.Client;

namespace LendFoundry.Business.Application
{
    public interface IApplicationConfiguration:IDependencyConfiguration
    {
         string InitialStatus { get; set; }

         int LocExpiryInMonths { get; set; }
         string ConnectionString { get; set; }
        
    }
}