﻿using LendFoundry.Business.Applicant;

namespace LendFoundry.Business.Application
{
    public interface IApplicationExtension
    {
        IApplication Application { get; set; }

        IApplicant Applicant { get; set; }

        string StatusWorkFlowId { get; set; }
    }
}