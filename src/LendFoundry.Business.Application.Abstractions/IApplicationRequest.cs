﻿using System.Collections.Generic;
using LendFoundry.Business.Applicant;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Date;
using Newtonsoft.Json;

namespace LendFoundry.Business.Application
{
    public interface IApplicationRequest
    {
        string ProductId { get; set; }
        string ContactFirstName { get; set; }
        string ContactLastName { get; set; }
        double RequestedAmount { get; set; }
        string LoanTimeFrame { get; set; }
        string RequestedTermType { get; set; }
        double RequestedTermValue { get; set; }
        string PurposeOfLoan { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ISource, Source>))]
        ISource Source { get; set; }

        string ApplicationNumber { get; set; }

        string PropertyType { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ISelfDeclareInformation, SelfDeclareInformation>))]
        ISelfDeclareInformation SelfDeclareInformation { get; set; }

        TimeBucket ApplicationDate { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IApplicantRequest, ApplicantRequest>))]
        IApplicantRequest PrimaryApplicant { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPhoneNumber, PhoneNumber>))]
        IPhoneNumber PrimaryPhone { get; set; }

        string PrimaryFax { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IEmailAddress, EmailAddress>))]
        IEmailAddress PrimaryEmail { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        IAddress PrimaryAddress { get; set; }

        TimeBucket DateNeeded { get; set; }

        string Signature { get; set; }
        string LeadOwnerId { get; set; }
        string PartnerId { get; set; }
        string PartnerUserId { get; set; }
        string OtherPurposeDescription { get; set; }
        string LeadOwnerEmail { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IExternalReferences, ExternalReferences>))]
        List<IExternalReferences> ExternalReferences { get; set; }

        double TotalDrawDownAmount { get; set; }
        double LoanAmount { get; set; }
        string ClientIpAddress { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPhoneNumber, PhoneNumber>))]
        IPhoneNumber BusinessPhone { get; set; }

        string OtherLenderNotes { get; set; }

        TimeBucket ProductApprovalDate { get; set; }
        string ParentApplicationNumber { get; set; }
        int SequenceNumber { get; set; }
        double RenewalAmount { get; set; }
        TimeBucket RenewedOn { get; set; }
        string RenewedBy { get; set; }
        string RenewalSource { get; set; }
        string ApplicationType { get; set; }
        string OriginalParentApplicationNumber { get; set; }

        string PortfolioType { get; set; }
        string ProductCategory { get; set; }
        string OldPortfolioType { get; set; }
        string OldProductCategory { get; set; }
    }
}