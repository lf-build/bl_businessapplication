﻿using System.Collections.Generic;
using System.Threading.Tasks;


namespace LendFoundry.Business.Application
{
    public interface IApplicationService
    {
        Task<IApplicationResponse> Add(IApplicationRequest applicationRequest);

        Task<IApplicationResponse> AddMerchantApplication(IApplicationRequest request);

        Task<IApplication> GetByApplicationId(string applicationId);

        Task<IApplication> GetByApplicationNumber(string applicationId);

        Task LinkBankInformation(string applicationId, IBankInformation bankInformationRequest);

        Task LinkEmailInformation(string applicationId, IEmailAddress emailInformationRequest);

        Task SetSelfDeclaredInformation(string applicationId, ISelfDeclareInformation declareInformationRequest);

        Task<IApplicationResponse> UpdateApplication(string applicationId, IApplicationRequest applicationRequest);

        Task<IApplicationDetails> GetApplicationDetails(string applicationId);

        Task<IApplication> AddExternalSources(string applicationNumber, IExternalReferencesRequest externalReferences);

        Task<List<IExternalReferences>> GetExternalSources(string applicationNumber);
        Task<List<IApplication>> GetApplicationByApplicantId(string applicantId);
        Task<IApplication> SwitchProduct(IApplication application);
        Task UpdateDrawDownAmount(string applicationNumber, IUpdateDrawDownAmountRequest requestAmount);
        Task UpdateLoanAmount(string applicationNumber, IUpdateLoanAmountRequest requestLoanAmount);
        Task<IApplication> UpdateFields(string applicationId, Dictionary<string, object> applicationFields);
        Task SetPrimary(string applicationId, string ownerId);
      
        Task<IApplicationResponse> AddRenewalApplication(string applicationNumber,
            IApplicationRequest applicationRequest);

        Task<List<IApplication>> GetRelatedApplications(string applicationId);
    }
}