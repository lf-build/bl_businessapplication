﻿using System;

namespace LendFoundry.Business.Application
{
    public interface IBankInformation
    {
        string AccountHolderName { get; set; }
        string AccountNumber { get; set; }

        AccountType AccountType { get; set; }

        string BankId { get; set; }
        string BankName { get; set; }

        bool IsVerified { get; set; }

        bool IsDefault { get; set; }
        string RoutingNumber { get; set; }

        string VerifiedBy { get; set; }
        DateTimeOffset VerifiedDate { get; set; }
    }
}