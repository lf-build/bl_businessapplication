﻿namespace LendFoundry.Business.Application
{
    public interface IExternalReference
    {
        string Name { get; set; }
        string Value { get; set; }
    }
}