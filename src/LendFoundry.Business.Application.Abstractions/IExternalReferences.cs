﻿using System.Collections.Generic;

namespace LendFoundry.Business.Application
{
    public interface IExternalReferences
    {
        string Name { get; set; }
        List<string> Value { get; set; }
    }
}