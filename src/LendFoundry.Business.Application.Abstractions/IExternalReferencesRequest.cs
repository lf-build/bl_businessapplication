﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Business.Application
{
    public interface IExternalReferencesRequest
    {
        [JsonConverter(typeof(InterfaceListConverter<IExternalReference, ExternalReference>))]
        List<IExternalReference> ExternalReferences { get; set; }
    }
}