﻿namespace LendFoundry.Business.Application
{
    public interface ISelfDeclareInformation
    {
        double AnnualRevenue { get; set; }

        double AverageBankBalance { get; set; }
        bool IsExistingBusinessLoan { get; set; }
    }
}