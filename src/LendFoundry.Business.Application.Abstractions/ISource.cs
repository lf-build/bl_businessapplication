﻿namespace LendFoundry.Business.Application
{
    public interface ISource
    {
        string TrackingCode { get; set; }

        /// <summary>
        ///     Source Website which initiated the application
        /// </summary>
        //    SystemChannel SystemChannel { get; set; }

        string SourceType { get; set; } // Send empty for tenant generated application

        string SourceReferenceId { get; set; }
    }
}