﻿namespace LendFoundry.Business.Application
{
    public interface IUpdateDrawDownAmountRequest
    {
        double DrawDownAmount { get; set; }
    }
}