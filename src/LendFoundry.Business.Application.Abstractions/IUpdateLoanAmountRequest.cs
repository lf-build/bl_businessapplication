﻿namespace LendFoundry.Business.Application
{
    public interface IUpdateLoanAmountRequest
    {
        double LoanAmount { get; set; }
    }
}