﻿namespace LendFoundry.Business.Application
{
    public interface IUpdateOtherLendersData
    {
        string OtherLenderNotes { get; set; }
    }
}