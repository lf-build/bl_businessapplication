﻿namespace LendFoundry.Business.Application
{
    public class PhoneNumber : IPhoneNumber
    {
        #region Constructors

        #endregion

        #region Public Properties

        public string Phone { get; set; }
        public string CountryCode { get; set; }
        public PhoneType PhoneType { get; set; }
        public bool IsDefault { get; set; }

        #endregion
    }
}