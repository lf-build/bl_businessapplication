﻿namespace LendFoundry.Business.Application
{
    public enum PhoneType
    {
        Residence = 1,
        Mobile = 2,
        Work = 3,
        Alternate = 4
    }
}