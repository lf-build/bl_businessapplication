﻿using System;
using System.Collections.Generic;
using LendFoundry.Business.Applicant;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Date;
using Newtonsoft.Json;

namespace LendFoundry.Business.Application
{
    public class ApplicationDetails : IApplicationDetails
    {
        public ApplicationDetails()
        {
        }

        public ApplicationDetails(IApplication application, IApplicant applicant)
        {
            if (application == null)
                throw new ArgumentNullException(nameof(application));
            if (applicant == null)
                throw new ArgumentNullException(nameof(applicant));

            if (application.PrimaryEmail != null)
                Email = application.PrimaryEmail.Email;
            ApplicationDate = application.ApplicationDate;
            ApplicantId = application.ApplicantId;
            ApplicationNumber = application.ApplicationNumber;
            RequestedAmount = application.RequestedAmount;
            PurposeOfLoan = application.PurposeOfLoan;
            LoanTimeFrame = application.LoanTimeFrame;
            DateNeeded = application.DateNeeded;
            ContactFirstName = application.ContactFirstName;
            ContactLastName = application.ContactLastName;
            if (application.Source != null)
            {
                Source = new Source();
                Source.SourceType = application.Source.SourceType;
                Source.SourceReferenceId = application.Source.SourceReferenceId;
                Source.TrackingCode = application.Source.TrackingCode;
            }

            Industry = applicant.Industry;

            if (application.PrimaryPhone != null) Phone = application.PrimaryPhone.Phone;
            if (application.BusinessPhone != null) BusinessPhone = application.BusinessPhone.Phone;
            if (application.PrimaryAddress != null)
            {
                AddressLine1 = application.PrimaryAddress.AddressLine1;
                City = application.PrimaryAddress.City;
                Country = application.PrimaryAddress.Country;
                State = application.PrimaryAddress.State;
                ZipCode = application.PrimaryAddress.ZipCode;
                AddressType = application.PrimaryAddress.AddressType.ToString();
            }

            if (application.SelfDeclareInformation != null)
            {
                AnnualRevenue = application.SelfDeclareInformation.AnnualRevenue;
                AverageBankBalances = application.SelfDeclareInformation.AverageBankBalance;
                HaveExistingLoan = application.SelfDeclareInformation.IsExistingBusinessLoan;
            }

            LegalBusinessName = applicant.LegalBusinessName;
            BusinessTaxID = applicant.BusinessTaxID;
            SICCode = applicant.SICCode;
            BusinessStartDate = applicant.BusinessStartDate;
            BusinessType = applicant.BusinessType;
            PropertyType = applicant.PropertyType;
            DBA = applicant.DBA;
            EIN = applicant.EIN;
            BusinessWebsite = applicant.BusinessWebsite;
            LoanPriority = applicant.LoanPriority;
            BusinessLocation = applicant.BusinessLocation;
            LeadOwnerId = application?.LeadOwnerId;
            PartnerId = application?.PartnerId;
            PartnerUserId = application?.PartnerUserId;
            if (applicant.Owners != null && applicant.Owners.Count > 0)
            {
                Owners = new List<IOwner>();

                foreach (var item in applicant.Owners)
                {
                    var objOwner = new Owner
                    {
                        OwnerId = item.OwnerId,
                        FirstName = item.FirstName,
                        LastName = item.LastName,
                        Designation = item.Designation,
                        DOB = item.DOB,
                        OwnershipPercentage = item.OwnershipPercentage,
                        OwnershipType = item.OwnershipType,
                        SSN = item.SSN,
                        IsPrimary = item.IsPrimary
                    };
                    if (item.Addresses != null && item.Addresses.Count > 0)
                    {
                        objOwner.Addresses = new List<Applicant.IAddress>();
                        foreach (var address in item.Addresses)
                        {
                            var objAddress = new Applicant.Address
                            {
                                AddressLine1 = address.AddressLine1,
                                City = address.City,
                                Country = address.Country,
                                State = address.State,
                                ZipCode = address.ZipCode
                            };
                            objOwner.Addresses.Add(objAddress);
                        }
                    }

                    if (item.PhoneNumbers != null && item.PhoneNumbers.Count > 0)
                    {
                        objOwner.PhoneNumbers = new List<Applicant.IPhoneNumber>();
                        foreach (var phone in item.PhoneNumbers)
                        {
                            var objPhone = new Applicant.PhoneNumber
                            {
                                Phone = phone.Phone,
                                PhoneType = phone.PhoneType
                            };
                            objOwner.PhoneNumbers.Add(objPhone);
                        }
                    }

                    if (item.EmailAddresses != null && item.EmailAddresses.Count > 0)
                    {
                        objOwner.EmailAddresses = new List<Applicant.IEmailAddress>();
                        foreach (var email in item.EmailAddresses)
                        {
                            var objEmail = new Applicant.EmailAddress
                            {
                                Email = email.Email
                            };
                            objOwner.EmailAddresses.Add(objEmail);
                        }
                    }

                    Owners.Add(objOwner);
                }
            }
        }

        public string Email { get; set; }
        public double RequestedAmount { get; set; }
        public string PurposeOfLoan { get; set; }
        public string LoanTimeFrame { get; set; }
        public TimeBucket DateNeeded { get; set; }
        public string LegalBusinessName { get; set; }
        public string AddressLine1 { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string AddressType { get; set; }
        public string BusinessTaxID { get; set; }
        public string SICCode { get; set; }
        public TimeBucket BusinessStartDate { get; set; }
        public string BusinessType { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public double AnnualRevenue { get; set; }
        public double AverageBankBalances { get; set; }
        public bool HaveExistingLoan { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ISource, Source>))]
        public ISource Source { get; set; }

        public string Industry { get; set; }
        public string Phone { get; set; }
        public string Signature { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IOwner, Owner>))]
        public IList<IOwner> Owners { get; set; }

        public string PropertyType { get; set; }
        public string ApplicationNumber { get; set; }
        public string ApplicantId { get; set; }
        public string DBA { get; set; }
        public string EIN { get; set; }
        public string LeadOwnerId { get; set; }
        public string PartnerUserId { get; set; }
        public string PartnerId { get; set; }
        public string BusinessPhone { get; set; }
        public string BusinessWebsite { get; set; }
        public string LoanPriority { get; set; }
        public string BusinessLocation { get; set; }
        public TimeBucket ApplicationDate { get; set; }
        public object ExtractedField { get; set; } 
    }
}