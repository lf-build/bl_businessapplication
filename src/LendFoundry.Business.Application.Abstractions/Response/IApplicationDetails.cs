﻿using System.Collections.Generic;
using LendFoundry.Business.Applicant;
using LendFoundry.Foundation.Date;

namespace LendFoundry.Business.Application
{
    public interface IApplicationDetails
    {
        string Email { get; set; }
        double RequestedAmount { get; set; }
        string PurposeOfLoan { get; set; }
        string LoanTimeFrame { get; set; }
        TimeBucket DateNeeded { get; set; }
        string LegalBusinessName { get; set; }
        string AddressLine1 { get; set; }
        string City { get; set; }
        string Country { get; set; }
        string State { get; set; }
        string ZipCode { get; set; }
        string AddressType { get; set; }
        string BusinessTaxID { get; set; }
        string SICCode { get; set; }
        TimeBucket BusinessStartDate { get; set; }
        string BusinessType { get; set; }
        string ContactFirstName { get; set; }
        string ContactLastName { get; set; }
        double AnnualRevenue { get; set; }
        double AverageBankBalances { get; set; }
        bool HaveExistingLoan { get; set; }
        ISource Source { get; set; }
        string Industry { get; set; }
        string Phone { get; set; }
        string Signature { get; set; }
        IList<IOwner> Owners { get; set; }
        string PropertyType { get; set; }
        string ApplicationNumber { get; set; }
        string ApplicantId { get; set; }
        string DBA { get; set; }
        string EIN { get; set; }
        string PartnerUserId { get; set; }
        string PartnerId { get; set; }
        string LeadOwnerId { get; set; }
        string BusinessPhone { get; set; }
        string BusinessWebsite { get; set; }
        string LoanPriority { get; set; }
        string BusinessLocation { get; set; }
        TimeBucket ApplicationDate { get; set; }
        object ExtractedField { get; set; } 
    }
}