﻿namespace LendFoundry.Business.Application
{
    public class SelfDeclareInformation : ISelfDeclareInformation
    {
        public SelfDeclareInformation()
        {
        }

        public SelfDeclareInformation(ISelfDeclareInformation expenseRequest)
        {
            AnnualRevenue = expenseRequest.AnnualRevenue;
            AverageBankBalance = expenseRequest.AverageBankBalance;
            IsExistingBusinessLoan = expenseRequest.IsExistingBusinessLoan;
        }

        public double AnnualRevenue { get; set; }

        public double AverageBankBalance { get; set; }
        public bool IsExistingBusinessLoan { get; set; }
    }
}