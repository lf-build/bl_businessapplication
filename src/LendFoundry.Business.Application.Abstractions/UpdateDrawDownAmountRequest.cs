﻿namespace LendFoundry.Business.Application
{
    public class UpdateDrawDownAmountRequest : IUpdateDrawDownAmountRequest
    {
        public double DrawDownAmount { get; set; }
    }
}