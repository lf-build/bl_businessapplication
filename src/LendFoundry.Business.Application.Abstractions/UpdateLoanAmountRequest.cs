﻿namespace LendFoundry.Business.Application
{
    public class UpdateLoanAmountRequest : IUpdateLoanAmountRequest
    {
        public double LoanAmount { get; set; }
    }
}