﻿namespace LendFoundry.Business.Application
{
    public class UpdateOtherLendersData : IUpdateOtherLendersData
    {
        public string OtherLenderNotes { get; set; }
    }
}