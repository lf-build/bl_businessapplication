﻿using LendFoundry.Security.Tokens;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Business.Application.Client
{
    public static class ApplicationServiceClientExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddApplicationService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IApplicationServiceClientFactory>(p => new ApplicationServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IApplicationServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
        public static IServiceCollection AddApplicationService(this IServiceCollection services, Uri uri)
        {
            services.AddSingleton<IApplicationServiceClientFactory>(p => new ApplicationServiceClientFactory(p, uri));
            services.AddSingleton(p => p.GetService<IApplicationServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddApplicationService(this IServiceCollection services)
        {
            services.AddSingleton<IApplicationServiceClientFactory>(p => new ApplicationServiceClientFactory(p));
            services.AddSingleton(p => p.GetService<IApplicationServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
