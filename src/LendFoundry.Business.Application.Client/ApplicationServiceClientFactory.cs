using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Business.Application.Client
{
    public class ApplicationServiceClientFactory : IApplicationServiceClientFactory
    {
        #region Constructors
        public ApplicationServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }
       public ApplicationServiceClientFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }

        
       
    #endregion

    #region Private Properties
       private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }
        private Uri Uri { get; }
        #endregion

        #region Public Methods
        public IApplicationService Create(ITokenReader reader)
        {
           var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("business_application");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new ApplicationService(client);
        }
        #endregion
    }
}
