﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Business.Application.Client
{
    public interface IApplicationServiceClientFactory
    {
        IApplicationService Create(ITokenReader reader);
    }
}
