using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using LendFoundry.StatusManagement;
using LendFoundry.Business.Applicant;
using LendFoundry.Business.Application.Events;
using LendFoundry.Business.Application.Persistence;
using LendFoundry.DataAttributes;
using LendFoundry.EventHub;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.NumberGenerator;


namespace LendFoundry.Business.Application {
    public class ApplicationService : IApplicationService {
        #region Constructor

        public ApplicationService
            (
                IApplicantService applicantService,
                IApplicationRepository applicationRepository,
                IGeneratorService applicationNumberGenerator,
                ILogger logger,
                ILookupService lookupService,
                IEventHubClient eventHubClient,
                ApplicationConfiguration applicationConfiguration,
                ITenantTime tenantTime,
                IEntityStatusService entityStatusService,
                IDataAttributesEngine dataAttributesEngine
            ) {
                ApplicantService = applicantService;
                ApplicationRepository = applicationRepository;
                ApplicationNumberGenerator = applicationNumberGenerator;
                EventHubClient = eventHubClient;
                CommandExecutor = new CommandExecutor (logger);
                if (applicationConfiguration == null)
                    throw new ArgumentNullException (nameof (applicationConfiguration));
                ApplicationConfigurations = applicationConfiguration;
                TenantTime = tenantTime;
                LookupService = lookupService;
                EntityStatusService = entityStatusService;
                DataAttributesEngine = dataAttributesEngine;
                Logger = logger;
            }

        #endregion Constructor

        #region Private Properties

        private IDataAttributesEngine DataAttributesEngine { get; }
        private ILookupService LookupService { get; }
        private IApplicantService ApplicantService { get; }
        private IEntityStatusService EntityStatusService { get; }
        private IApplicationRepository ApplicationRepository { get; }
        private IGeneratorService ApplicationNumberGenerator { get; }
        private IEventHubClient EventHubClient { get; }
        private ITenantTime TenantTime { get; }
        private CommandExecutor CommandExecutor { get; }
        private ApplicationConfiguration ApplicationConfigurations { get; }
        private ILogger Logger { get; }
        #endregion Private Properties

        #region PrivateMethods

        private async Task<IApplicationExtension> SaveApplication (IApplicationRequest request, string applicationStatus) {
            Application application = null;
            IApplicant applicant = null;
           
            var applicationExtension = new ApplicationExtension ();
                string ApplicantId=string.Empty;
            
            await Task.Run (() => {
                application = new Application (request);
                var addApplicantCommand = new Command (
                    execute: () => {
                        ApplicantRequest applicantRequest = new ApplicantRequest (request.PrimaryApplicant);
                        applicant = ApplicantService.Add (applicantRequest).Result;
                        applicationExtension.Applicant = applicant;
                        ApplicantId = applicant.Id;
                    },
                    rollback: () => ApplicantService.Delete (application.ApplicantId)
                );
                 CommandExecutor.Execute (new List<Command> () { addApplicantCommand});
             });

            string applicantNumber = ApplicationNumberGenerator.TakeNext ("application").Result;
            request.OriginalParentApplicationNumber=applicantNumber;
            request.ParentApplicationNumber=applicantNumber;
             if (string.IsNullOrWhiteSpace (applicantNumber))
                             throw new Exception ("Unable to generate application number");
            var Owners = SetOwners (applicationExtension.Applicant.Owners);
            var applicationResult=await AddApplication(request,applicantNumber,Owners,ApplicantId);
           
            applicationResult.Applicant=applicationExtension.Applicant;
            return applicationResult;
        }

        private List<string> SetOwners (IList<IOwner> owners) {
            List<string> Owner = new List<string> ();
            if (owners != null && owners.Any ()) {
                foreach (var ownerdata in owners) {
                    Owner.Add (ownerdata.OwnerId);
                }
            }
            return Owner;
        }

        private async Task<IApplicant> GetApplicant (string applicantId) {
            if (string.IsNullOrWhiteSpace (applicantId))
                throw new ArgumentNullException ($"{nameof(applicantId)} cannot be null");

            var applicant = await ApplicantService.Get (applicantId);

            if (applicant == null)
                throw new NotFoundException ($"Applicant {applicant} not found");

            return applicant;
        }

         private async Task<IApplicationExtension> SaveRenewalApplication (string applicationNumber,IApplicationRequest request, string applicationStatus) {
            var  applicationDetail = await GetByApplicationNumber(applicationNumber);
          

            request.OriginalParentApplicationNumber=(string.IsNullOrWhiteSpace(applicationDetail.ParentApplicationNumber))?applicationDetail.ApplicationNumber:applicationDetail.OriginalParentApplicationNumber;
            var Owners = applicationDetail.Owners;
            var renewalApplicationNumber=ApplicationNumberGenerator.TakeNext ("renewalApplication",new List<string>(){request.OriginalParentApplicationNumber}).Result;
            string newapplicationNumber =request.OriginalParentApplicationNumber+"-"+ renewalApplicationNumber;
            request.ParentApplicationNumber=applicationNumber;
            request.ApplicationType= ApplicationType.Renewal.ToString();
            var applicationExtension= await AddApplication(request,newapplicationNumber,Owners,applicationDetail.ApplicantId);
          
            applicationExtension.Applicant=await GetApplicant(applicationDetail.ApplicantId);
            return applicationExtension;
        }

        private async Task<IApplicationExtension> AddApplication (IApplicationRequest request, string applicationNumber,List<string> owners,string applicantId) {
            Application application = null;
            //IApplicant applicant = null;
           
            var applicationExtension = new ApplicationExtension ();
            await Task.Run (() => {
                application = new Application (request);
               
                var addApplicationCommand = new Command (
                    execute: () => {
                        application.Owners = owners;
                        application.ApplicantId=applicantId;
                        application.ApplicationNumber = applicationNumber;
                        application.PortfolioType = Convert.ToString (request.PortfolioType);
                        application.ProductCategory = Convert.ToString (request.ProductCategory);
                        application.ApplicationDate = new TimeBucket (TenantTime.Now);
                        application.ExpiryDate = new TimeBucket (TenantTime.Now.AddDays (ApplicationConfigurations.ExpiryDays));
                        ApplicationRepository.Add (application);
                        applicationExtension.Application = application;
                    },
                    rollback: () => ApplicationRepository.Remove (application)
                );
                CommandExecutor.Execute (new List<Command> () { addApplicationCommand });
            });

            return applicationExtension;
        }

        #endregion PrivateMethods

        #region PublicMethods

        public async Task<IApplicationResponse> Add (IApplicationRequest request) {
            IApplicationExtension applicationExtension = await SaveApplication (request, ApplicationConfigurations.InitialStatus);

            await EventHubClient.Publish (new ApplicationCreated () {
                Application = applicationExtension.Application,
                    Applicant = applicationExtension.Applicant,
                    EntityId = applicationExtension.Application.ApplicationNumber,
                    EntityType = "application"
            });
            return new ApplicationResponse (applicationExtension.Application, applicationExtension.StatusWorkFlowId);
        }

         public async Task<IApplicationResponse> AddRenewalApplication (string applicationNumber,IApplicationRequest request) {
            IApplicationExtension applicationExtension = await SaveRenewalApplication (applicationNumber,request, ApplicationConfigurations.InitialStatus);

            await EventHubClient.Publish (new ApplicationCreated () {
                Application = applicationExtension.Application,
                    Applicant = applicationExtension.Applicant,
                    EntityId = applicationExtension.Application.ApplicationNumber,
                    EntityType = "application"
            });
            return new ApplicationResponse (applicationExtension.Application, applicationExtension.StatusWorkFlowId);
        }

        // Not sure for the Business Application it is needed or not
        public async Task<IApplicationResponse> AddMerchantApplication (IApplicationRequest request) {
            IApplicationExtension applicationExtension = await SaveApplication (request, ApplicationConfigurations.InitialLeadStatus);

            await EventHubClient.Publish (new ApplicationCreated () {
                Application = applicationExtension.Application,
                    Applicant = applicationExtension.Applicant,
                    EntityId = applicationExtension.Application.ApplicationNumber,
                    EntityType = "application"
            });

            return new ApplicationResponse (applicationExtension.Application, applicationExtension.StatusWorkFlowId);
        }

        public async Task<IApplication> GetByApplicationId (string applicationId) {
            if (string.IsNullOrWhiteSpace (applicationId))
                throw new ArgumentNullException ($"{nameof(applicationId)} cannot be null");

            var application = await ApplicationRepository.GetByApplicationId (applicationId);

            if (application == null)
                throw new NotFoundException ($"Application {applicationId} not found");

            return application;
        }

        public async Task<IApplication> GetByApplicationNumber (string applicationId) {
            if (string.IsNullOrWhiteSpace (applicationId))
                throw new ArgumentNullException ($"{nameof(applicationId)} cannot be null");

            var application = await ApplicationRepository.GetByApplicationId (applicationId);

            if (application == null)
                throw new NotFoundException ($"Application {applicationId} not found");

            return application;
        }

        public async Task LinkBankInformation (string applicationNumber, IBankInformation bankInformationRequest) {
            EnsureInputIsValid (applicationNumber, bankInformationRequest);
            var application = await ApplicationRepository.LinkBankInformation (applicationNumber, bankInformationRequest);
            var applicant = await GetApplicant (application.ApplicantId);
            await EventHubClient.Publish (new ApplicationModified () { Application = application, Applicant = applicant });
        }

        public async Task LinkEmailInformation (string applicationNumber, IEmailAddress emailInformationRequest) {
            EnsureInputIsValid (applicationNumber, emailInformationRequest);
            var application = await ApplicationRepository.LinkEmailInformation (applicationNumber, emailInformationRequest);
            var applicant = await GetApplicant (application.ApplicantId);
            await EventHubClient.Publish (new ApplicationModified () { Application = application, Applicant = applicant });
        }

        public async Task SetSelfDeclaredInformation (string applicationNumber, ISelfDeclareInformation delcareInformationRequest) {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            if (delcareInformationRequest == null)
                throw new ArgumentNullException (nameof (delcareInformationRequest));

            var application = await ApplicationRepository.SetSelfDeclaredInformation (applicationNumber, delcareInformationRequest);
            var applicant = await GetApplicant (application.ApplicantId);
            await EventHubClient.Publish (new ApplicationModified () { Application = application, Applicant = applicant });
        }

        private async Task<IApplicationExtension> Edit (string applicationNumber, IApplicationRequest request) {
            var applicationExtension = new ApplicationExtension ();
            IApplication application = null;
            IApplicant applicant = null;
            var applicantId = string.Empty;
            if (request.PrimaryApplicant != null && !string.IsNullOrEmpty (request.PrimaryApplicant.Id)) {
                applicantId = request.PrimaryApplicant.Id;
            }
            application = new Application (request);
            application.ApplicationNumber = applicationNumber;
            await Task.Run (() => {
                var updateApplicantCommand = new Command (
                    execute: () => {
                        IUpdateApplicantRequest applicantRequest = new UpdateApplicantRequest (request.PrimaryApplicant);
                        applicant = ApplicantService.UpdateApplicant (applicantId, applicantRequest).Result;
                        applicant.Id = applicantId;
                        applicationExtension.Applicant = applicant;
                        request.PrimaryApplicant.Owners = applicant.Owners;
                    },
                    rollback: () => ApplicantService.Delete (applicantId)
                );
                var updateApplicationCommand = new Command (
                    execute: () => {
                        IApplicationUpdateRequest applicationUpdate = new ApplicationUpdateRequest (request);
                        applicationUpdate.ApplicationDate = new TimeBucket (TenantTime.Now);
                        var objApplication = ApplicationRepository.UpdateApplication (applicationNumber, applicationUpdate);
                        applicationExtension.Application = objApplication.Result;
                    },
                    rollback: () => ApplicationRepository.Remove (application)
                );
                CommandExecutor.Execute (new List<Command> () { updateApplicantCommand, updateApplicationCommand });
            });

            return applicationExtension;
        }

        public async Task<IApplicationResponse> UpdateApplication (string applicationNumber, IApplicationRequest request) {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new Exception ("Application number is mandantory");

            IApplicationExtension applicationExtension = await Edit (applicationNumber, request);

            await EventHubClient.Publish (new ApplicationModified () { Application = applicationExtension.Application, Applicant = applicationExtension.Applicant });

            return new ApplicationResponse (applicationExtension.Application, applicationExtension.StatusWorkFlowId);
        }

        public async Task<IApplication> AddExternalSources (string applicationNumber, IExternalReferencesRequest externalReferences) {
            Logger.Info ($"Started Add External Source At:{TenantTime.Now.UtcDateTime} for App#:{applicationNumber}");
            var applicationDetails = await GetByApplicationNumber (applicationNumber);

            if (externalReferences == null)
                throw new ArgumentNullException (nameof (externalReferences));

            foreach (var item in externalReferences.ExternalReferences) {

                ExternalReferences objExternalReference = new ExternalReferences ();
                objExternalReference.Name = item.Name;
                objExternalReference.Value = new List<string> ();
                objExternalReference.Value.Add (item.Value);

                if (applicationDetails.ExternalReferences == null) {
                    applicationDetails.ExternalReferences = new List<IExternalReferences> ();
                }

                var isalreadyExists = applicationDetails.ExternalReferences.FirstOrDefault (i => i.Name.ToLower () == item.Name.ToLower ());
                if (isalreadyExists != null)
                    applicationDetails.ExternalReferences.Remove (isalreadyExists);
                applicationDetails.ExternalReferences.Add (objExternalReference);

            }

            ApplicationRepository.Update (applicationDetails);
            Logger.Info ($"Ended Add External Source At:{TenantTime.Now.UtcDateTime} for App#:{applicationNumber}");
            await EventHubClient.Publish (new ApplicationModified () { Application = applicationDetails, Applicant = await GetApplicant (applicationDetails.ApplicantId) });

            return applicationDetails;
        }

        public async Task<List<IExternalReferences>> GetExternalSources (string applicationNumber) {
            var applicationDetails = await GetByApplicationNumber (applicationNumber);

            return applicationDetails.ExternalReferences;
        }

        public async Task<IApplicationDetails> GetApplicationDetails (string applicationId) {
            if (string.IsNullOrWhiteSpace (applicationId))
                throw new ArgumentNullException ($"{nameof(applicationId)} cannot be null");

            var application = await ApplicationRepository.GetByApplicationId (applicationId);

            if (application == null)
                throw new NotFoundException ($"Application {applicationId} not found");

            var applicant = await GetApplicant (application.ApplicantId);

            if (applicant == null)
                throw new NotFoundException ($"Applicant {applicant} not found");

            return new ApplicationDetails (application, applicant);
        }
        public async Task<List<IApplication>> GetApplicationByApplicantId (string applicantId) {
            if (string.IsNullOrWhiteSpace (applicantId))
                throw new ArgumentNullException ($"{nameof(applicantId)} cannot be null");

            return await ApplicationRepository.GetApplicationByApplicantId (applicantId);
        }

        public async Task<IApplication> SwitchProduct (IApplication application) {
           if ((application == null))
                throw new ArgumentNullException ($"{nameof(application)} cannot be null");
            
            TimeBucket LocExpiryDate = new TimeBucket (TenantTime.Now.AddMonths (ApplicationConfigurations.LocExpiryInMonths));

            await DataAttributesEngine.SetAttribute ("application", application.ApplicationNumber, "ProductApprovalDate", application.ProductApprovalDate);
            await DataAttributesEngine.SetAttribute ("application", application.ApplicationNumber, "LocExpiryDate", LocExpiryDate);
            ApplicationRepository.Update (application);

            var applicant = await GetApplicant (application.ApplicantId);
            await EventHubClient.Publish (new ApplicationModified () { Application = application, Applicant = applicant });
            return application;
        }

        public async Task UpdateLoanAmount (string applicationNumber, IUpdateLoanAmountRequest requestLoanAmount) {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            if (requestLoanAmount == null)
                throw new ArgumentNullException (nameof (requestLoanAmount));

            if (requestLoanAmount.LoanAmount <= 0)
                throw new InvalidOperationException ("RequestLoanAmount should be grater than zero");
            var application = await GetByApplicationNumber (applicationNumber);

            if (application == null)
                throw new NotFoundException ($"Application with number {applicationNumber} is not found");

            application.LoanAmount = requestLoanAmount.LoanAmount;
            ApplicationRepository.Update (application);

            var applicant = await ApplicantService.Get (application.ApplicantId);
            await EventHubClient.Publish (new ApplicationModified { Application = application, Applicant = applicant });
        }

        public async Task UpdateDrawDownAmount (string applicationNumber, IUpdateDrawDownAmountRequest requestAmount) {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            if (requestAmount == null)
                throw new ArgumentNullException (nameof (requestAmount));

            if (requestAmount.DrawDownAmount <= 0)
                throw new InvalidOperationException ("DrawDownAmount should be grater than zero");

            var application = await GetByApplicationNumber (applicationNumber);

            if (application == null)
                throw new NotFoundException ($"Application with number {applicationNumber} is not found");
            var oldLoanAmount = application.RequestedAmount;
            application.TotalDrawDownAmount = requestAmount.DrawDownAmount;
            ApplicationRepository.Update (application);
            var applicant = await ApplicantService.Get (application.ApplicantId);
            await EventHubClient.Publish (new ApplicationModified { Application = application, Applicant = applicant });
        }

        public async Task<IApplication> UpdateFields (string applicationId, Dictionary<string, object> applicationFields) {
            if (string.IsNullOrWhiteSpace (applicationId))
                throw new ArgumentNullException (nameof (applicationId));
            if (applicationFields == null || applicationFields.Count == 0)
                throw new ArgumentNullException (nameof (applicationFields));
            await ApplicationRepository.UpdateFields (applicationId, applicationFields);
            return await GetByApplicationId (applicationId);
        }

        public async Task SetPrimary (string applicationId, string ownerId) {
            if (string.IsNullOrWhiteSpace (applicationId))
                throw new ArgumentNullException (nameof (applicationId));
            if (string.IsNullOrWhiteSpace (ownerId))
                throw new ArgumentNullException (nameof (ownerId));

            var application = await ApplicationRepository.GetByApplicationId (applicationId);

            await ApplicantService.SetPrimary (application.ApplicantId, ownerId);

            var applicant = await GetApplicant (application.ApplicantId);
            await EventHubClient.Publish (new ApplicationModified () { Application = application, Applicant = applicant });
        }
           
          public async Task<List<IApplication>> GetRelatedApplications (string applicationId) {
            if (string.IsNullOrWhiteSpace (applicationId))
                throw new ArgumentNullException ($"{nameof(applicationId)} cannot be null");
        
            var applications = await ApplicationRepository.GetRelatedApplications (applicationId);

            if (applications == null)
                throw new NotFoundException ($"Application {applicationId} not found");

            return applications;
        }
        #endregion PublicMethods

        #region Validations

        private void EnsureInputIsValid (IApplicationRequest applicationRequest) {
            if (applicationRequest == null)
                throw new ArgumentNullException ("application cannot be empty");

            if (applicationRequest.RequestedAmount <= 0)
                throw new ArgumentNullException ($"Application {nameof(applicationRequest.RequestedAmount)} is mandatory");
            var PurposeOfLoanExists = LookupService.GetLookupEntry ("purposeOfLoan", applicationRequest.PurposeOfLoan);
            if (PurposeOfLoanExists == null)
                throw new InvalidArgumentException ("PurposeOfLoan is not valid");
            var LoantimeFrameExists = LookupService.GetLookupEntry ("loanTimeFrame", applicationRequest.LoanTimeFrame);
            if (LoantimeFrameExists == null)
                throw new InvalidArgumentException ("LoanTimeFrame is not valid");

            var SourceTypeExists = LookupService.GetLookupEntry ("sourceType", applicationRequest.Source.SourceType);
            if (PurposeOfLoanExists == null)
                throw new InvalidArgumentException ("SourceType is not valid");

            if (applicationRequest.RequestedTermType != null) {
                var RequestedTermTypeExists = LookupService.GetLookupEntry ("requestedTermType", applicationRequest.RequestedTermType);

                if (RequestedTermTypeExists == null)
                    throw new InvalidArgumentException ("RequestedTermType is not valid");
            }
            if (applicationRequest.RequestedTermValue >= 0.0) {
                var RequestedTermValueExists = LookupService.GetLookupEntry ("RequestedTermValue", applicationRequest.RequestedTermValue.ToString ());
                if (RequestedTermValueExists == null)
                    throw new InvalidArgumentException ("RequestedTermValue is not valid");
            }
            if (applicationRequest.Source == null)
                throw new ArgumentNullException ($"Application {nameof(applicationRequest.Source)} is mandatory");

            if (string.IsNullOrWhiteSpace (applicationRequest.Source.SourceReferenceId))
                throw new ArgumentNullException ($"Application {nameof(applicationRequest.Source.SourceReferenceId)} is mandatory");

            if (applicationRequest.PrimaryEmail == null)
                throw new ArgumentNullException ($"Applicant {nameof(applicationRequest.PrimaryEmail)} is mandatory");
            if (!Regex.IsMatch (applicationRequest.PrimaryEmail.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
                throw new ArgumentException ("Email Address is not valid");
            if (applicationRequest.PrimaryPhone == null)
                throw new ArgumentNullException ($"Applicant {nameof(applicationRequest.PrimaryPhone)} is mandatory");

            if (!Regex.IsMatch (applicationRequest.PrimaryPhone.Phone, "^(0|[1-9][0-9]+)$"))
                throw new ArgumentException ("Invalid Phone Number format");

            if (applicationRequest.PrimaryApplicant == null)
                throw new ArgumentNullException ($"Applicant {nameof(applicationRequest.PrimaryApplicant)} is mandatory");

            if (applicationRequest.PrimaryAddress == null)
                throw new ArgumentNullException ($"Applicant {nameof(applicationRequest.PrimaryAddress)} is mandatory");

            if (applicationRequest.PrimaryApplicant.Owners == null)
                throw new ArgumentNullException ($"Applicant {nameof(applicationRequest.PrimaryApplicant.Owners)} is mandatory");

            foreach (var Owners in applicationRequest.PrimaryApplicant.Owners) {
                if (string.IsNullOrWhiteSpace (Owners.FirstName))
                    throw new ArgumentNullException ($"Applicant {nameof(Owners.FirstName)} is mandatory");
                if (string.IsNullOrWhiteSpace (Owners.SSN))
                    throw new ArgumentNullException ($"Applicant {nameof(Owners.SSN)} is mandatory");
                if (string.IsNullOrWhiteSpace (Owners.DOB))
                    throw new ArgumentNullException ($"Applicant {nameof(Owners.DOB)} is mandatory");
                if (string.IsNullOrWhiteSpace (Owners.OwnershipPercentage))
                    throw new ArgumentNullException ($"Applicant {nameof(Owners.OwnershipPercentage)} is mandatory");
                if (!Enum.IsDefined (typeof (OwnershipType), Owners.OwnershipType))
                    throw new ArgumentNullException ($"Applicant {nameof(Owners.OwnershipType)} is mandatory");
                if (!Enum.IsDefined (typeof (OwnershipType), Owners.Addresses.Where (x => x.AddressType.ToString () == "Home").Select (x => x.City)))
                    throw new ArgumentNullException ($"Applicant {nameof(Owners.OwnershipType)} is mandatory");
            }
            if (applicationRequest.PrimaryApplicant.Addresses == null)
                throw new ArgumentNullException ($"Applicant {nameof(applicationRequest.PrimaryApplicant.Addresses)} is mandatory");

            foreach (var address in applicationRequest.PrimaryApplicant.Addresses) {
                if (string.IsNullOrWhiteSpace (address.AddressLine1))
                    throw new ArgumentNullException ($"Applicant {nameof(address.AddressLine1)} is mandatory");

                if (string.IsNullOrWhiteSpace (address.City))
                    throw new ArgumentNullException ($"Applicant {nameof(address.City)} is mandatory");

                if (string.IsNullOrWhiteSpace (address.State))
                    throw new ArgumentNullException ($"Applicant {nameof(address.State)} is mandatory");

                if (string.IsNullOrWhiteSpace (address.ZipCode))
                    throw new ArgumentNullException ($"Applicant {nameof(address.ZipCode)} is mandatory");

                if (string.IsNullOrWhiteSpace (address.Country))
                    throw new ArgumentNullException ($"Applicant {nameof(address.Country)} is mandatory");

                if (!Enum.IsDefined (typeof (Applicant.AddressType), address.AddressType))
                    throw new InvalidEnumArgumentException ($"{nameof(address.AddressType)} is not valid");
            }
            if (applicationRequest.PrimaryApplicant.PhoneNumbers == null)
                throw new ArgumentNullException ($"Applicant {nameof(applicationRequest.PrimaryApplicant.PhoneNumbers)} is mandatory");

            foreach (var phoneNumber in applicationRequest.PrimaryApplicant.PhoneNumbers) {
                if (string.IsNullOrWhiteSpace (phoneNumber.Phone))
                    throw new ArgumentNullException ($"Applicant {nameof(PhoneNumber.Phone)} is mandatory");
                if (!Regex.IsMatch (phoneNumber.Phone, "^(0|[1-9][0-9]+)$"))
                    throw new ArgumentException ("Invalid Phone Number format");
                if (!Enum.IsDefined (typeof (Applicant.PhoneType), phoneNumber.PhoneType))
                    throw new InvalidEnumArgumentException ($"{nameof(phoneNumber.PhoneType)} is not valid");
            }

            if (applicationRequest.PrimaryApplicant.EmailAddresses == null)
                throw new ArgumentNullException ($"Applicant {nameof(applicationRequest.PrimaryApplicant.EmailAddresses)} is mandatory");

            foreach (var emailAddress in applicationRequest.PrimaryApplicant.EmailAddresses) {
                if (string.IsNullOrWhiteSpace (emailAddress.Email))
                    throw new ArgumentNullException ($"Applicant {nameof(EmailAddress.Email)} is mandatory");
                if (!Regex.IsMatch (emailAddress.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
                    throw new ArgumentException ("Email Address is not valid");

                if (!Enum.IsDefined (typeof (Applicant.EmailType), emailAddress.EmailType))
                    throw new InvalidEnumArgumentException ($"{nameof(emailAddress.EmailType)} is not valid");
            }
        }

        private void EnsureInputIsValid (string applicationNumber, IBankInformation bankInformationRequest) {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            if (bankInformationRequest == null)
                throw new ArgumentNullException (nameof (bankInformationRequest));

            if (string.IsNullOrWhiteSpace (bankInformationRequest.BankName))
                throw new ArgumentNullException ($"{nameof(bankInformationRequest.BankName)} is mandatory");

            if (string.IsNullOrWhiteSpace (bankInformationRequest.AccountHolderName))
                throw new ArgumentNullException ($"{nameof(bankInformationRequest.AccountHolderName)} is mandatory");

            if (string.IsNullOrWhiteSpace (bankInformationRequest.AccountNumber))
                throw new ArgumentNullException ($"{nameof(bankInformationRequest.AccountNumber)} is mandatory");

            if (string.IsNullOrWhiteSpace (bankInformationRequest.RoutingNumber))
                throw new ArgumentNullException ($"{nameof(bankInformationRequest.RoutingNumber)} is mandatory");

            if (!Enum.IsDefined (typeof (AccountType), bankInformationRequest.AccountType))
                throw new InvalidEnumArgumentException ($"{nameof(bankInformationRequest.AccountType)} is mandatory");
        }

        private void EnsureInputIsValid (string applicationNumber, IEmailAddress emailInformationRequest) {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            if (emailInformationRequest == null)
                throw new ArgumentNullException (nameof (emailInformationRequest));

            if (string.IsNullOrWhiteSpace (emailInformationRequest.Email))
                throw new ArgumentNullException ($"{nameof(emailInformationRequest.Email)} is mandatory");

            if (!Enum.IsDefined (typeof (EmailType), emailInformationRequest.EmailType))
                throw new InvalidEnumArgumentException ($"{nameof(emailInformationRequest.EmailType)} is mandatory");
        }

        #endregion Validations
    }
  

}