﻿using LendFoundry.Business.Applicant;
using LendFoundry.Business.Application.Api.Controllers;
using LendFoundry.Business.Application.Tests.Fakes;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Client;
using Microsoft.AspNet.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.Business.Application.Api.Tests
{
    public class ApplicationControllerTest
    {
        #region Public  Methods
        [Fact]
        public async Task Application_Should_Add_With_Valid_Data()
        {
            var applicantService = GetApplicantService();
         
            var response = await GetController(applicantService).Add(GetApplicationRequestData());

            Assert.IsType<HttpOkObjectResult>(response);
            var application = ((HttpOkObjectResult)response).Value as IApplicationResponse;

            Assert.NotNull(application);
            Assert.NotNull(application.ApplicationNumber);
            Assert.Equal("1", application.ApplicationNumber);
            Assert.Equal("1", application.ApplicationNumber);
            Assert.Equal(60, (application.ExpiryDate - application.ApplicationDate).Days);
          
            //check if applicant is inserted to separate collection/repository
            var applicantData = applicantService.Get(application.ApplicantId).Result;
            Assert.NotNull(applicantData);
        
        }


        [Fact]
        public void Should_Get_Application_By_ApplicationNumber()
        {
            List<IApplication> applications = GetApplicationData();
            Mock<IApplicantService> applicantService = new Mock<IApplicantService>();


            var response = GetController(applicantService.Object, applications).Get("1").Result;

            Assert.IsType<HttpOkObjectResult>(response);
            var application = ((HttpOkObjectResult)response).Value as IApplication;

            Assert.NotNull(application);
            Assert.NotNull(application.ApplicationNumber);
            Assert.Equal("1", application.ApplicationNumber);
        }

        [Fact]
        public void Should_LinkBankInformation_By_ApplicationNumber()
        {
            List<IApplication> applications = GetApplicationData();
            var bankInfo = new BankInformation()
            {
                BankName = "Axis",
                AccountNumber = "1223344556",
                AccountHolderName = "SIS",
                RoutingNumber = "Ifsc001",
                AccountType = AccountType.Savings
            };

            var applicantService = GetApplicantService();
            var response = GetController(applicantService, applications).LinkBankInformation("1", bankInfo).Result;

            Assert.IsType<NoContentResult>(response);
        }

        [Fact]
        public void LinkBankInformation_Should_Return_Bad_Request_For_InValid_Data()
        {
            List<IApplication> applications = GetApplicationData();

            var bankInfo = new BankInformation()
            {
                BankName = "Axis",
                AccountNumber = "1223344556",
                AccountHolderName = "SIS",
                RoutingNumber = "Ifsc001",
                AccountType = AccountType.Checking
            };

            var applicantService = GetApplicantService();
            var response = GetController(applicantService, applications).LinkBankInformation("", bankInfo).Result;

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public void LinkBankInformation_Should_ReturnNotFound_For_ApplicationNumberNotExists()
        {
            List<IApplication> applications = GetApplicationData();

            var bankInfo = new BankInformation()
            {
                BankName = "Axis",
                AccountNumber = "1223344556",
                AccountHolderName = "SIS",
                RoutingNumber = "Ifsc001",
                AccountType = AccountType.Savings
            };

            var applicantService = GetApplicantService();
            var response = GetController(applicantService, applications).LinkBankInformation("1234", bankInfo).Result;

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(404, result.StatusCode);
        }


        [Fact]
        public void LinkEmailInformation_Should_Save_Data_ToApplicationSuccess()
        {
            List<IApplication> applications = GetApplicationData();

            var emailInfo = new EmailAddress()
            {
                Email = "testemail@sigmainfo.net",
                IsDefault = false,
                IsVerified = false,
                EmailType = EmailType.Personal
            };

            var applicantService = GetApplicantService();
            var response = GetController(applicantService, applications).LinkEmailInformation("1", emailInfo).Result;

            Assert.IsType<NoContentResult>(response);
        }

        [Fact]
        public void LinkEmailInformation_Should_ReturnBadRequest_ForInvliadData()
        {
            List<IApplication> applications = GetApplicationData();
            var emailInfo = new EmailAddress()
            {
                Email = "testemail@sigmainfo.net",
                IsDefault = false,
                IsVerified = false,
            };

            var applicantService = GetApplicantService();
            var response = GetController(applicantService, applications).LinkEmailInformation("1", emailInfo).Result;

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public void LinkEmailInformation_Should_ReturnNotFound_ForApplicationNumberNotExists()
        {
            List<IApplication> applications = GetApplicationData();

            var emailInfo = new EmailAddress()
            {
                Email = "testemail@sigmainfo.net",
                IsDefault = false,
                IsVerified = false,
                EmailType = EmailType.Personal
            };

            var applicantService = GetApplicantService();
            var response = GetController(applicantService, applications).LinkEmailInformation("abcd", emailInfo).Result;

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(404, result.StatusCode);
        }


        [Fact]
        public void SetSelfDeclaredExpense_Should_Save_ToApplicationSuccess()
        {
           List<IApplication> applications = GetApplicationData();

            var expense = new SelfDeclareInformation()
            {
                AnnualRevenue = 300000,
                AverageBankBalance = 10000,
                IsExistingBusinessLoan = true               
            };

            var applicantService = GetApplicantService();
            var response = GetController(applicantService, applications).LinkSelfDeclaredExpense("1", expense).Result;

            Assert.IsType<NoContentResult>(response);
        }

        [Fact]
        public void SetSelectedInitialOffer_Should_Success()
        {
            List<IApplication> applications = GetApplicationData();

            var applicantService = GetApplicantService();
            var response = GetController(applicantService, applications).SetSelectedOffer("1", "f01", "initialoffer").Result;
            Assert.IsType<NoContentResult>(response);
        }

        [Fact]
        public void SetInitialOffer_Should_Save_ToApplicationSuccess()
        {
            List<IApplication> applications = GetApplicationData();

            var initialOffers = new List<LoanOffer>()
            {
                 new LoanOffer()
                    {
                        AnnualPercentageRate = 15.90,
                        DisbursedLoanAmount = 199000,
                        GivenLoanAmount = 200000,
                        InterestRate = 10,
                        IsSelectedOffer = false,
                        NumberOfIntallments = 60,
                        OfferFees = new List<IOfferFee>
                        {
                            new OfferFee()
                             {
                                FeeAmount = 10000,
                                FeePercentage = 5,
                                FeeType = OfferFeeType.OneTime,
                                IsIncludedInLoanAmount = true
                            }
                        },
                        RepaymentFrequency = LoanFrequency.Monthly,
                        RepaymentInstallmentAmount = 15000
                    },
                  new LoanOffer()
                    {
                        AnnualPercentageRate = 10.23,
                        DisbursedLoanAmount = 199000,
                        GivenLoanAmount = 200000,
                        InterestRate = 12,
                        IsSelectedOffer = false,
                        NumberOfIntallments = 36,
                        OfferFees = new List<IOfferFee>
                        {
                            new OfferFee()
                             {
                                FeeAmount = 10000,
                                FeePercentage = 5,
                                FeeType = OfferFeeType.OneTime,
                                IsIncludedInLoanAmount = true
                            }
                        },
                        RepaymentFrequency = LoanFrequency.Monthly,
                        RepaymentInstallmentAmount = 25000
                    }
            };

            var applicantService = GetApplicantService();
            var response = GetController(applicantService, applications).SetOffers("1", "initialoffer", initialOffers).Result;

            Assert.IsType<NoContentResult>(response);
        }

        [Fact]
        public void SetFinalOffer_Should_Save_ToApplicationSuccess()
        {
            List<IApplication> applications = GetApplicationData();

            var finalOffers = new List<LoanOffer>()
            {
                 new LoanOffer()
                    {
                        AnnualPercentageRate = 15.90,
                        DisbursedLoanAmount = 199000,
                        GivenLoanAmount = 200000,
                        InterestRate = 10,
                        IsSelectedOffer = false,
                        NumberOfIntallments = 60,
                        OfferFees = new List<IOfferFee>
                        {
                            new OfferFee()
                             {
                                FeeAmount = 10000,
                                FeePercentage = 5,
                                FeeType = OfferFeeType.OneTime,
                                IsIncludedInLoanAmount = true
                            }
                        },
                        RepaymentFrequency = LoanFrequency.Monthly,
                        RepaymentInstallmentAmount = 15000
                    },
                  new LoanOffer()
                    {
                        AnnualPercentageRate = 10.23,
                        DisbursedLoanAmount = 199000,
                        GivenLoanAmount = 200000,
                        InterestRate = 12,
                        IsSelectedOffer = false,
                        NumberOfIntallments = 36,
                        OfferFees = new List<IOfferFee>
                        {
                            new OfferFee()
                             {
                                FeeAmount = 10000,
                                FeePercentage = 5,
                                FeeType = OfferFeeType.OneTime,
                                IsIncludedInLoanAmount = true
                            }
                        },
                        RepaymentFrequency = LoanFrequency.Monthly,
                        RepaymentInstallmentAmount = 25000
                    }
            };

            var applicantService = GetApplicantService();
            var response = GetController(applicantService, applications).SetOffers("1", "finaloffer", finalOffers).Result;

            Assert.IsType<NoContentResult>(response);
        }

        [Fact]
        public void SetOffer_Should_ReturnsBadRequest_ForInvalidOfferType()
        {
            List<IApplication> applications = GetApplicationData();
            var initialOffers = new List<LoanOffer>()
            {
                 new LoanOffer()
                    {
                        AnnualPercentageRate = 15.90,
                        DisbursedLoanAmount = 199000,
                        GivenLoanAmount = 200000,
                        InterestRate = 10,
                        IsSelectedOffer = false,
                        NumberOfIntallments = 60,
                        OfferFees = new List<IOfferFee>
                        {
                            new OfferFee()
                             {
                                FeeAmount = 10000,
                                FeePercentage = 5,
                                FeeType = OfferFeeType.OneTime,
                                IsIncludedInLoanAmount = true
                            }
                        },
                        RepaymentFrequency = LoanFrequency.Monthly,
                        RepaymentInstallmentAmount = 15000
                    },
                  new LoanOffer()
                    {
                        AnnualPercentageRate = 10.23,
                        DisbursedLoanAmount = 199000,
                        GivenLoanAmount = 200000,
                        InterestRate = 12,
                        IsSelectedOffer = false,
                        NumberOfIntallments = 36,
                        OfferFees = new List<IOfferFee>
                        {
                            new OfferFee()
                             {
                                FeeAmount = 10000,
                                FeePercentage = 5,
                                FeeType = OfferFeeType.OneTime,
                                IsIncludedInLoanAmount = true
                            }
                        },
                        RepaymentFrequency = LoanFrequency.Monthly,
                        RepaymentInstallmentAmount = 25000
                    }
            };

            var applicantService = GetApplicantService();
            var response = GetController(applicantService, applications).SetOffers("1", "offer", initialOffers).Result;

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }


        [Fact]
        public void GetInitialOffer_Should_Return_Application()
        {

            List<IApplication> applications = GetApplicationData();

            var applicantService = GetApplicantService();
            var response = GetController(applicantService, applications).GetApplicationOffers("1", "initialoffer").Result;
            Assert.IsType<HttpOkObjectResult>(response);
        }
        #endregion

        #region Private Methods
        private static ApplicationController GetController(List<IApplication> applications = null, IEnumerable<IApplicant> applicants = null)
        {
            var applicantService = new FakeApplicantService(new UtcTenantTime(), applicants ?? new List<IApplicant>());

            return new ApplicationController(GetApplicationService(applicantService, applications));
        }

        private static ApplicationController GetController(IApplicantService applicantService, IEnumerable<IApplication> applications = null)
        {
            return new ApplicationController(GetApplicationService(applicantService, applications));
        }
        private static ApplicationController GetController(IApplicationService applicationService, IApplicantService applicantService)
        {
            return new ApplicationController(applicationService);
        }

        private static IApplicantService GetApplicantService(IEnumerable<IApplicant> applicants = null)
        {
            return new FakeApplicantService(new UtcTenantTime(), applicants ?? new List<IApplicant>());
        }

        private static IApplicationService GetApplicationService(IApplicantService applicantService, IEnumerable<IApplication> applications = null)
        {
            return new ApplicationService(applicantService,
                new FakeApplicationRepository(new UtcTenantTime(), applications ?? new List<IApplication>()), new FakeApplicationNumberGenerator(),
                Mock.Of<ILogger>(), Mock.Of<IEventHubClient>(), new ApplicationConfiguration() { ExpiryDays = 60, InitialLeadStatus = "200.01", InitialStatus = "200.02" }, Mock.Of<ITenantTime>());
        }
        //private static IApplicationService GetApplicationService(IApplicantService applicantService,IApplicationRepository applicationRepository ,List<IApplication> applications = null)
        //{
        //    return new ApplicationService(applicantService,
        //        applicationRepository, new FakeApplicationNumberGenerator(),
        //        Mock.Of<ILogger>(), Mock.Of<IEventHubClient>(), new ApplicationConfiguration() { ExpiryDays = 60, InitialLeadStatus = "200.01", InitialStatus = "200.02" }, Mock.Of<IEntityStatusService>(), Mock.Of<ITenantTime>());
        //}

        private static List<IApplication> GetApplicationData()
        {
            return new List<IApplication>
            {

                new Application()
            {

                Source = new Source()
                {
                    SourceType = SourceType.Merchant,

                    TrackingCode = "Code123",
                    SourceReferenceId = "Refe123"
                },

                RequestedAmount = 10000,
                RequestedTermType = LoanFrequency.Monthly,
                RequestedTermValue = 12.4333,
                ApplicationNumber = "1",
                ApplicationStatus = "200.02",
                PrimaryEmail = new EmailAddress(),
                 PrimaryAddress = new Address(),
                 PrimaryFax = "testFax",
                  PrimaryPhone = new PhoneNumber(),
                LinkedBankInformation = new BankInformation()
                {
                        BankName = "HDFC",
                        AccountNumber = "567907845",
                        AccountHolderName = "SigmaInfo",
                        RoutingNumber = "ifsc1234",
                        AccountType =  AccountType.Savings
                },
                LinkedEmailAddress = new EmailAddress()
                {
                    Email = "myemail@sigmainfo.net",
                    IsDefault = true,
                    IsVerified = false,
                    EmailType = EmailType.Work
                },
              SelfDeclareInformation = new SelfDeclareInformation()
                {
                    AnnualRevenue =1000,
                      AverageBankBalance = 2000,
                       IsExistingBusinessLoan = true
                },
                InitialOffers = new List<ILoanOffer>
                {
                    new LoanOffer()
                    {
                        AnnualPercentageRate = 10.23,
                        DisbursedLoanAmount = 199000,
                        GivenLoanAmount = 200000,
                        InterestRate = 10,
                        IsSelectedOffer = true,
                        NumberOfIntallments = 10,
                        OfferId = "f01",
                        OfferFees = new List<IOfferFee>
                        {
                            new OfferFee()
                             {
                                FeeAmount = 10000,
                                FeePercentage = 5,
                                FeeType = OfferFeeType.OneTime,
                                IsIncludedInLoanAmount = true
                            }
                        },
                        RepaymentFrequency = LoanFrequency.Monthly,
                        RepaymentInstallmentAmount = 15000
                    }
                },
                 FinalOffers = new List<ILoanOffer>
                {
                    new LoanOffer()
                    {
                        AnnualPercentageRate = 10.23,
                        DisbursedLoanAmount = 199000,
                        GivenLoanAmount = 200000,
                        InterestRate = 10,
                        IsSelectedOffer = false,
                        NumberOfIntallments = 10,
                        OfferFees = new List<IOfferFee>
                        {
                            new OfferFee()
                             {
                                FeeAmount = 10000,
                                FeePercentage = 5,
                                FeeType = OfferFeeType.OneTime,
                                IsIncludedInLoanAmount = true
                            }
                        },

                        RepaymentFrequency = LoanFrequency.Monthly,
                        RepaymentInstallmentAmount = 15000
                    }
                }
            }};
        }


        private ApplicationRequest GetApplicationRequestData()
        {
            return new ApplicationRequest
            {
                PrimaryApplicant = new ApplicantRequest()
                {
                    PrimaryAddress = new LendFoundry.Business.Applicant.Address(),
                    PrimaryEmail = new LendFoundry.Business.Applicant.EmailAddress(),
                    PrimaryFax = "test",
                    PrimaryPhone = new LendFoundry.Business.Applicant.PhoneNumber(),
                    Addresses = new List<Applicant.IAddress>
                     {
                         new Applicant.Address()
                         {
                             AddressLine1 = "Test1",
                             AddressLine2 = "Test2",
                             City = "Ahmedabad",
                             State = "Gujarat",
                             PinCode = "380001",
                             Country = "India",
                              AddressType = Applicant.AddressType.Home
                         }
                     },

                    EmailAddresses = new List<Applicant.IEmailAddress>
                     {
                         new Applicant.EmailAddress
                         {
                             Email = "Test@SigmaInfo.net",
                             EmailType = Applicant.EmailType.Personal,
                             IsDefault = true
                         }
                     },
                    Owners = new List<Applicant.IOwner>
                     {
                         new Owner
                         {
                              DOB = new DateTimeOffset().ToString(),
                                FirstName = "Test First Name",
                                LastName = "Test Last Name",
                                SSN = "Test",
                                 Ownership = "TT",
                                  Designation = "Mr",
                                   OwnershipType = OwnershipType.OwnershipType1
                         }
                     },
                    UserName = "TestUserExists1",
                    Password = "TestPassword",
                    PhoneNumbers = new List<Applicant.IPhoneNumber>
                     {
                    new Applicant.PhoneNumber()
                    {
                        Phone = "9898989898",
                        PhoneType = Applicant.PhoneType.Residence,
                        CountryCode = "91"
                    }
                    },

                },
                PrimaryEmail = new EmailAddress(),
                PrimaryAddress = new Address(),
                PrimaryFax = "testFax",
                PrimaryPhone = new PhoneNumber(),
                PurposeOfLoan = PurposeOfLoan.DebtConsolidation,
                Source = new Source()
                {
                    SourceType = SourceType.Merchant,

                    TrackingCode = "Code123",
                    SourceReferenceId = "Refe111"
                },

                RequestedAmount = 10000,
                RequestedTermType = LoanFrequency.Monthly,
                RequestedTermValue = 12.4333
            };
        }
        #endregion
    }
}
