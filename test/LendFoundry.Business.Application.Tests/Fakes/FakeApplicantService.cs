﻿using LendFoundry.Business.Applicant;
using LendFoundry.Foundation.Date;
using LendFoundry.Security.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Business.Application.Tests.Fakes
{
    public class FakeApplicantService : IApplicantService
    {
        #region Constructors
        public FakeApplicantService(ITenantTime tenantTime, IEnumerable<IApplicant> applicants) : this(tenantTime)
        {
            Applicants.AddRange(applicants);
            CurrentId = Applicants.Count() + 1;
        }

        public FakeApplicantService(ITenantTime tenantTime)
        {
            TenantTime = tenantTime;
            CurrentId = 1;
        }
        #endregion

        #region Private Properties
        private ITenantTime TenantTime { get; set; }
        public List<IApplicant> Applicants { get; } = new List<IApplicant>();
        public int CurrentId { get; set; }
        #endregion

        #region Public Methods
        public async Task<IApplicant> Add(IApplicantRequest applicantRequest)
        {
            IApplicant applicant = null;
            await Task.Run(() =>
            {
                if (applicantRequest.UserName == "TestUserExists")
                    throw new UsernameAlreadyExists("TestUserExists");
                applicant = new Applicant.Applicant(applicantRequest);
                applicant.Id = (CurrentId++).ToString();
                Applicants.Add(applicant);
                //response = new Applicant.Applicant(applicantRequest);
            });
            return applicant;
        }

        public void Update(string applicantId, IApplicant applicant)
        {
            Applicants.Remove(Applicants.First(a => a.Id.Equals(applicant.Id)));
            Applicants.Add(applicant);
        }

        public async Task<IApplicant> Get(string applicantId)
        {
            IApplicant applicantDetail = null;
            await Task.Run(() =>
            {
                applicantDetail = Applicants.First(applicant => applicant.Id.Equals(applicantId));
            });
            return applicantDetail;
        }

        public async Task Delete(string applicantId)
        {
            await Task.Run(() =>
            {
                var applicant = Applicants.Where(a => a.Id.Equals(applicantId)).FirstOrDefault();
                Applicants.Remove(applicant);
            });
        }

        public Task UpdateApplicant(string applicantNumber, IUpdateApplicantRequest applicantRequest)
        {
            throw new NotImplementedException();
        }

        public Task SetAddresses(string applicantId, List<IAddress> addresses)
        {
            throw new NotImplementedException();
        }

        public Task SetPhoneNumbers(string applicantId, List<IPhoneNumber> phoneNumbers)
        {
            throw new NotImplementedException();
        }

        public Task SetEmailAddresses(string applicantId, List<IEmailAddress> emailAddresses)
        {
            throw new NotImplementedException();
        }

        public Task SetAddresses(string applicantId, List<Applicant.IAddress> addresses)
        {
            throw new NotImplementedException();
        }

        public Task AssociateUser(string applicantId, string userId)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAddresses(string applicantId, List<Applicant.IAddress> addresses)
        {
            throw new NotImplementedException();
        }

        public Task UpdatePhoneNumbers(string applicantId, List<Applicant.IPhoneNumber> phoneNumbers)
        {
            throw new NotImplementedException();
        }

        public Task UpdateEmailAddresses(string applicantId, List<Applicant.IEmailAddress> emailAddresses)
        {
            throw new NotImplementedException();
        }

        public Task SetPhoneNumbers(string applicantId, List<Applicant.IPhoneNumber> phoneNumbers)
        {
            throw new NotImplementedException();
        }

        public Task SetEmailAddresses(string applicantId, List<Applicant.IEmailAddress> emailAddresses)
        {
            throw new NotImplementedException();
        }

     

        public Task SetBanks(string applicantId, List<Applicant.IBankInformation> banks)
        {
            throw new NotImplementedException();
        }
        Task IApplicantService.Delete(string applicantId)
        {
            throw new NotImplementedException();
        }

        Task IApplicantService.AssociateUser(string applicantId, string userId)
        {
            throw new NotImplementedException();
        }

        Task IApplicantService.UpdateApplicant(string applicantNumber, IUpdateApplicantRequest applicantRequest)
        {
            throw new NotImplementedException();
        }

        Task IApplicantService.SetAddresses(string applicantId, List<Applicant.IAddress> addresses)
        {
            throw new NotImplementedException();
        }

        Task IApplicantService.SetPhoneNumbers(string applicantId, List<Applicant.IPhoneNumber> phoneNumbers)
        {
            throw new NotImplementedException();
        }

        Task IApplicantService.SetEmailAddresses(string applicantId, List<Applicant.IEmailAddress> emailAddresses)
        {
            throw new NotImplementedException();
        }

        Task IApplicantService.SetOwner(string applicantId, List<IOwner> ownres)
        {
            throw new NotImplementedException();
        }

        Task IApplicantService.SetBanks(string applicantId, List<Applicant.IBankInformation> banks)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
